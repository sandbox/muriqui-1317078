<?php
/**
 * @file
 * Admin UI for the media upload directory module.
 */

/**
 * The module configuration form.
 */
function media_upload_directory_admin($form, &$form_state) {
  $form = array();
  $types = file_info_file_types();
  foreach ($types as $type => $info) {
    $form['media_upload_directory_' . $type] = array(
      '#title' => t('Default directory for @label uploads', array('@label' => $info['label'])),
      '#type' => 'textfield',
      '#default_value' => variable_get('media_upload_directory_' . $type, ''),
      '#element_validate' => array('_file_generic_settings_file_directory_validate'),
    );
  }
  return system_settings_form($form);
}
